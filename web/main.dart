import 'package:angular/angular.dart';
import 'package:display_ip/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}
