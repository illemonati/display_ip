import 'package:angular/angular.dart';

import 'src/ip/ip_component.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: [IPComponent],
)
class AppComponent {
  // Nothing here yet. All logic is in TodoListComponent.
}
