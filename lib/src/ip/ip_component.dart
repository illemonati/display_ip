
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

import 'ip_service.dart';
import 'package:tuple/tuple.dart';

@Component(
  selector: 'ip',
  template: '''
    <div class="mdc-card">
      <p style="text-align: center">{{ip_and_port}}</p>
    </div>
    ''',
  providers: [ClassProvider(IPService)],
  styleUrls: [
    'package:angular_components/css/mdc_web/card/mdc-card.scss.css',
  ]
)
class IPComponent implements OnInit{
  String ip_and_port = 'Unknown Error';
  final IPService _ipService;
  IPComponent(this._ipService);
  ngOnInit() async {
    try {
      var ip_and_port = await _ipService.getIPandPort();
      var ip = ip_and_port.item1;
      var port = ip_and_port.item2;
      this.ip_and_port = '$ip:$port';
    } on Exception catch(e) {
      this.ip_and_port = 'Error: $e';
    }

    print(this.ip_and_port);
  }

}