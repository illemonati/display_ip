import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tuple/tuple.dart';

class IPService {
  
  Future<String> getIP() async {
    final res = await getRaw();
    return res['ip'];
  }

  Future<int> getPort() async {
    final res = await getRaw();
    var port = res['port'];
    return port;
  }

  Future<Tuple2<String, int>> getIPandPort() async {
    final res = await getRaw();
    var ip = res['ip'];
    var port = res['port'];
    return Tuple2<String, int>(ip, port);
  }

  Future<dynamic> getRaw() async {
    var url = 'https://ip.tioft.tech';
    var response = await http.get(url);
    var res = jsonDecode(response.body);
    return res;
  }




}